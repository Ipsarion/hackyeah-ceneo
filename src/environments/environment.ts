// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCGC2MbdnuhJysb5EPM4YvO5OuKawzuGyg",
    authDomain: "hackyeah-ceneo.firebaseapp.com",
    databaseURL: "https://hackyeah-ceneo.firebaseio.com",
    projectId: "hackyeah-ceneo",
    storageBucket: "hackyeah-ceneo.appspot.com",
    messagingSenderId: "393070683704",
    appId: "1:393070683704:web:86225d48d0b4667a5568cd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

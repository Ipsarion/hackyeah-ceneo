import { QuizGameComponent } from './components/quiz-game/quiz-game.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { QrCodeComponent } from './components/qr-code/qr-code.component';
import { GamePanelComponent } from './components/game-panel/game-panel.component';
import { UserSummaryComponent } from './components/user-summary/user-summary.component';
import { FriendsComponent } from './components/friends/friends.component';


const routes: Routes = [
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: 'main/:id',
    component: MainComponent
  },
  {
    path: 'Quiz',
    component: QuizGameComponent,
  },
  {
    path: 'qrCode', component: QrCodeComponent
  },
  {
    path: 'games', component: GamePanelComponent
  },
  { path: 'userSummary', component: UserSummaryComponent },
  {
    path: 'friends', component: FriendsComponent
  },
  { path: '**', component: MainComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { DatabaseService } from './services/database/database.service';
import { Observable } from 'rxjs';
import { Hackyeah } from './interfaces/hackyeah';
import { TrainService } from './services/train/train.service';
import { map, mapTo } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  hackyeah: Hackyeah;
  getData: Observable<any>;

  constructor (private database: DatabaseService, private trainService: TrainService ){}

  ngOnInit() {
  }
}

import { Injectable } from '@angular/core';
import { Train } from 'src/app/interfaces/train';
import { Capability } from 'protractor';
import { randomBytes } from 'crypto';
import { DatabaseService } from '../database/database.service';

@Injectable({
  providedIn: 'root'
})
export class TrainService {

  train: Train;
  cityA: Array<string> = ['Wrocław', 'Poznań', 'Kłodzko' ];
  cityB: Array<string> = ['Warszawa', 'Gdańsk', 'Kielce'];
  wagony: number;

  constructor(private databaseService: DatabaseService) { }
  
  generateTrain(ticketId: string){

    let wagonQuantity = this.getRandomInt(3, 8);

    this.train = {
      wagonQuantity: wagonQuantity,
      warsPosition: this.getRandomInt(2, wagonQuantity - 1),
      startCity: this.cityA[this.getRandomInt(0,this.cityA.length)],
      endCity: this.cityB[this.getRandomInt(0,this.cityB.length)],
      ticketId: ticketId,
      userWagon: this.getRandomInt(1, wagonQuantity),
    }
    this.databaseService.setPost('trains',ticketId, this.train);
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
}

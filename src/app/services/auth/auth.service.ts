import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { auth } from 'firebase/app';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth'
import { User } from 'src/app/interfaces/user';
import { DatabaseService } from '../database/database.service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user$: Observable<User>
  
  constructor(
    private afAuth: AngularFireAuth,
    private database: DatabaseService,
    private router: Router) {

    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          // return this.afs.doc('users/' + user.uid).valueChanges()
          return this.database.getDoc('users', user.uid );
        } else {
          return of(null);
        }

      })
    );
  }

  async googleSignin(){
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    this.updateUserData(credential.user)
  }

  async signOut(){
    console.log('signOut');
    await this.afAuth.auth.signOut();
    return this.router.navigate(['/']);
  }

  private updateUserData(credential){
    console.log(credential);
    const user: User = {
      uid: credential.uid,
      email: credential.email,
      displayName: credential.displayName,
      photoURL: credential.photoURL,
    }

    return this.database.setPost('users', credential.uid, user);
  }
}

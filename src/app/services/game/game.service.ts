import { Injectable } from '@angular/core';
import { DatabaseService } from '../database/database.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  gameId = 'IWccsgyVcXkqHpNploXo';

  constructor(private databaseService: DatabaseService ) { }

  private getCurrrentDateFormat() {
    let date = new Date();
    return date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + '_' + 
    date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
  }

  private isActiveGame() {
    var game = this.databaseService.getDoc('games', this.gameId);
    
    return game !== null;
  }

  initNewGame(train: string) {
    if(!this.isActiveGame()) 
    {
      var currentDate = this.getCurrrentDateFormat();
      this.databaseService.setPost('games', 'id', {
          start: currentDate,
          train: train,
          blueTeam: [],
          orangeTeam: []
      });
    }
  }
}

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private afs: AngularFirestore) { }

  setPost(collection: string, doc: string, post: any) {
    if (doc === 'id') {doc = this.afs.createId(); }
    this.afs.collection(collection).doc(doc).set(post, {merge: true});
    return doc;
  }

  getDoc(collection: string, doc: string): Observable<any> {
    return this.afs.collection(collection).doc(doc).valueChanges();
  }

  updatePost(collection: string, doc: string, post: any) {
    if (doc === 'id') {doc = this.afs.createId(); }
    this.afs.collection(collection).doc(doc).set(post, {merge:true});
    return doc;
  }
}

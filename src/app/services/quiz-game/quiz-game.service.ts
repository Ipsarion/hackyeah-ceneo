import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuizQuestion } from 'src/app/interfaces/quiz-question';
import { Observable, interval, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuizGameService {

  private pageNumber = 0;
  private quizQuestionsBehaviorSubject = new BehaviorSubject<QuizQuestion[]>([]);
  constructor(private http: HttpClient) {
    this.loadMoreQuizQuestions()
    interval(5000).subscribe(x => {
      console.log("load more")
      this.loadMoreQuizQuestions()
    })
  }

  loadMoreQuizQuestions() {
    this.http.get<QuizQuestion[]>('https://hackyeah-pkp.azurewebsites.net/api/GetQuizQuestions?pageNumber=' + this.pageNumber)
      .pipe(take(1))
      .subscribe(quizQuestions => {

        console.log(quizQuestions)
        this.quizQuestionsBehaviorSubject.next(quizQuestions)
        if (quizQuestions.length > 0)
          this.pageNumber++;
      })
  }

  getMoreQuizQuestions(): Observable<QuizQuestion[]> {
    return this.quizQuestionsBehaviorSubject
  }


}

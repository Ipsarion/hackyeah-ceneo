import { TestBed } from '@angular/core/testing';

import { QuizGameService } from './quiz-game.service';

describe('QuizGameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuizGameService = TestBed.get(QuizGameService);
    expect(service).toBeTruthy();
  });
});

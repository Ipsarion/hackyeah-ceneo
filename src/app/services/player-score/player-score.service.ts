import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerScoreService {

  currentScore: BehaviorSubject<number>

  constructor() {
    const scoreFromLocalStorage = this.getScore()
    this.currentScore = new BehaviorSubject(scoreFromLocalStorage)
  }

  getScore(): number {
    return Number(localStorage.getItem("score"))
  }

  addToScore(toAdd: number) {
    const scoreFromLocalStorage = this.getScore() ? this.getScore() : 0;
    const newScore = scoreFromLocalStorage + toAdd;
    localStorage.setItem("score", scoreFromLocalStorage + toAdd + '')
    this.currentScore.next(newScore)
  }
}

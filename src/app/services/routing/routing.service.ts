import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {
  constructor() {
  }

  public activeRouting = new Subject<any>()
}

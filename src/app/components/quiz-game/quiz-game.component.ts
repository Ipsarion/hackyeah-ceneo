import { QuizGameService } from './../../services/quiz-game/quiz-game.service';
import { Component, OnInit } from '@angular/core';
import { QuizQuestion } from 'src/app/interfaces/quiz-question';
import { interval } from 'rxjs';
import { PlayerScoreService } from 'src/app/services/player-score/player-score.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-quiz-game',
  templateUrl: './quiz-game.component.html',
  styleUrls: ['./quiz-game.component.scss']
})
export class QuizGameComponent implements OnInit {
  quizQuestions: QuizQuestion[] = []
  showCorrectAnswer: boolean = false;
  timeLeft: number;
  timeInterval: number;

  answeredCorrectly: Boolean = null;
  answerId: Number = null;
  tillEnd: number = 6;

  constructor(private router: Router, private quizGameService: QuizGameService, private playerScoreService: PlayerScoreService, private snackBar: MatSnackBar) {
    this.timeInterval = 15;
    this.timeLeft = this.timeInterval;
  }

  ngOnInit() {
    this.quizGameService.getMoreQuizQuestions().subscribe(x => {
      x.forEach(y => {
        this.quizQuestions.push(y)
      })
    })



    interval(1000).subscribe(x => {

      if (this.timeLeft < 0) {
        this.showCorrectAnswer = true
      }
      else {
        this.timeLeft--;
      }
    })

  }

  onClickAnswer(event, answerId: number) {
    this.timeLeft = 0;
    this.tillEnd--;
    if (this.tillEnd === 0) {
      this.router.navigateByUrl('/userSummary');
    }
    if (this.showCorrectAnswer === false) {
      event.stopPropagation();
      this.answerId = answerId;
      if (this.quizQuestions[0].correctAnswerId === answerId) {
        this.playerScoreService.addToScore(100)
        this.answeredCorrectly = true;
        let snackBarRef = this.snackBar.open(this.quizQuestions[0].moreInfo);
      }
      else {
        this.answeredCorrectly = false;
      }
    }
    this.showCorrectAnswer = true;
  }

  onClickNext() {
    if (this.showCorrectAnswer === true && this.quizQuestions.length > 0) {
      this.timeLeft = this.timeInterval;
      this.showCorrectAnswer = false;
      this.answeredCorrectly = null;
      this.answerId = null;
      this.quizQuestions.shift()
      this.snackBar.dismiss()
    }
  }
}

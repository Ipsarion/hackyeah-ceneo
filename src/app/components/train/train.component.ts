import { Component, OnInit, Input } from '@angular/core';
import { Train } from 'src/app/interfaces/train';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-train',
  templateUrl: './train.component.html',
  styleUrls: ['./train.component.scss']
})
export class TrainComponent implements OnInit {

  @Input()
  trainGoing: boolean = false;

  @Input()
  trainObservable: Observable<Train>;

  cars: Array<Number> = [1, 2, 3];
  isDefault: boolean = true;
  train: Train;
  gameMode: boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.trainObservable) {
      this.trainObservable.subscribe(v => {
        this.train = v;
        this.isDefault = false;
        this.cars = Array(v.wagonQuantity).fill(4).map((x, i)=>i);
        this.trainGoing = true;
      });
    }
  }

  startGame(){
    this.gameMode = !this.gameMode;
  }
}

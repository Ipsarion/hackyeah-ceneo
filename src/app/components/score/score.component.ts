import { PlayerScoreService } from './../../services/player-score/player-score.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  constructor(private playerScoreService: PlayerScoreService) { }

  playerScore: number = 0;

  ngOnInit() {
    this.playerScoreService.currentScore.subscribe(x => {
      this.playerScore = x
    })
  }

}

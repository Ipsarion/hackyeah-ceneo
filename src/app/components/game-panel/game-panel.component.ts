import { Component, OnInit } from '@angular/core';

enum GameType {
  Quiz,
  Maze
}

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.scss']
})
export class GamePanelComponent implements OnInit {

  public GameType = GameType;
  public currentGameType: GameType = GameType.Quiz
  gameDone: boolean = false;
  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { RoutingService } from 'src/app/services/routing/routing.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private routingService: RoutingService) { }
  appTitle: string = "HackYeah"
  title: string = this.appTitle; 


  ngOnInit() {
    this.routingService.activeRouting.subscribe( v => {
      if (v && v == 'qrCode') {this.title = "Skanuj kod z biletu"}
      }
    )
  }
}

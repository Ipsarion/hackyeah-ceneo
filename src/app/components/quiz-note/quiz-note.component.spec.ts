import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizNoteComponent } from './quiz-note.component';

describe('QuizNoteComponent', () => {
  let component: QuizNoteComponent;
  let fixture: ComponentFixture<QuizNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

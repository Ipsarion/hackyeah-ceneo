import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quiz-note',
  templateUrl: './quiz-note.component.html',
  styleUrls: ['./quiz-note.component.scss']
})
export class QuizNoteComponent implements OnInit {

  constructor() { }

  @Input() quizNote: string;

  ngOnInit() {
  }



}

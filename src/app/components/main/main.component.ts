import { Component, OnInit } from '@angular/core';
import { TrainService } from 'src/app/services/train/train.service';
import { Train } from 'src/app/interfaces/train';
import { ActivatedRoute } from '@angular/router';
import { GameService } from 'src/app/services/game/game.service';
import { DatabaseService } from 'src/app/services/database/database.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private gameService: GameService,
    private trainService: TrainService,
    private route: ActivatedRoute,
    private database: DatabaseService
  ) { }

  train: Train;
  routing: string;
  track: any[];

  trainObservable: Observable<Train>;

  ngOnInit() {
    this.route.params.subscribe(routing => {
      this.routing = routing.id;
      if (routing.id) {this.trainService.generateTrain(routing.id)}
    });

    this.track = [
      ["Wrocław", false],
      ["Kalisz", false],
      ["Warszawa", false]
    ]

    this.trainObservable = this.database.getDoc('trains', this.routing);

  }

  startGame() {
    this.gameService.initNewGame('intercityTrain');
  }

}

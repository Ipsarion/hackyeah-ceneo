import { Component, OnInit, Input } from '@angular/core';
import { PlayerScoreService } from 'src/app/services/player-score/player-score.service';

@Component({
  selector: 'app-user-summary',
  templateUrl: './user-summary.component.html',
  styleUrls: ['./user-summary.component.scss']
})
export class UserSummaryComponent implements OnInit {

  constructor(private service: PlayerScoreService) { }

score: number;

  ngOnInit() {
this.service.currentScore.subscribe(x => {
  this.score = x;
})
}

}

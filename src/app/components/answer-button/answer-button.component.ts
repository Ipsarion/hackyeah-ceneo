import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-answer-button',
  templateUrl: './answer-button.component.html',
  styleUrls: ['./answer-button.component.scss']
})
export class AnswerButtonComponent implements OnInit {
  constructor() { }
  @Input() answer: string;
  @Input() isCorrect: boolean;
  @Input() showBorder: boolean;

  color: string;
  ngOnInit() {
  }

  checkTheAnswer() {
    if (this.isCorrect) {
      this.color = 'green'
    }
    else {
      this.color = 'red'
    }
  }

}

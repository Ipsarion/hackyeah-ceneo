import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { QrCodeComponent } from './components/qr-code/qr-code.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AuthComponent } from './components/auth/auth.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { firebase } from '@firebase/app';

import {
  MatButtonModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule
} from '@angular/material';

import { TrainComponent } from './components/train/train.component';
import { MainComponent } from './components/main/main.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { QuizGameComponent } from './components/quiz-game/quiz-game.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { HttpClientModule } from '@angular/common/http';
import { QuizNoteComponent } from './components/quiz-note/quiz-note.component';
import { AnswerButtonComponent } from './components/answer-button/answer-button.component';
import { FormsModule } from '@angular/forms';
import { TrackComponent } from './components/track/track.component';
import { ScoreComponent } from './components/score/score.component';
import { GamePanelComponent } from './components/game-panel/game-panel.component';
import { UserSummaryComponent } from './components/user-summary/user-summary.component';
import { FriendsComponent } from './components/friends/friends.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    QrCodeComponent,
    ToolbarComponent,
    AuthComponent,
    TrainComponent,
    MainComponent,
    QuizGameComponent,
    TrackComponent,
    QuizNoteComponent,
    AnswerButtonComponent,
    ScoreComponent,
    GamePanelComponent,
    UserSummaryComponent,
    FriendsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    MatButtonModule,
    ZXingScannerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatGridListModule,
    MatStepperModule,
    HttpClientModule,
    MatProgressBarModule,
    FormsModule,
    MatListModule,
    MatChipsModule,
    MatCardModule,
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }

export interface Train{
    wagonQuantity: number;
    warsPosition: number;
    startCity: string,
    endCity: string,
    ticketId: string,
    userWagon: number;
}
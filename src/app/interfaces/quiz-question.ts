export interface QuizQuestion {
    questionText: string;
    correctAnswerId: number;
    answers: string[];
    moreInfo: string;
}
